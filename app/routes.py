# encoding: utf-8

from app import admin
from app.controller.admin import admin_routes
from app.controller.core import Core
from app.controller.representative import Representative


# Adding admin endpoints
for route in admin_routes:
    admin.add_view(route[0](name=route[1], endpoint=route[2], category=route[3]))

# Listing normal endpoints
routes = [
    ("/", Core.as_view("home")),
    ("/about", Core.as_view("about")),
    ("/representative", Representative.as_view("index"), ("GET", "POST")),
]

# Listing API endpoints
apis = []
