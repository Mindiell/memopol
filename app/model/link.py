# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Link(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("links", lazy="dynamic")
    )
    name = db.Column(db.String(2000))
    url = db.Column(db.String(2000))

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = [("representative.name", False), ("name", False)]
    column_filters = ["name", "representative.name"]


admin.add_view(AdminView(Link, db.session, category="CRUD"))
