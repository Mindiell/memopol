# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Log(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    category = db.Column(db.String(2000))
    message = db.Column(db.Text)


class AdminView(View):
    column_default_sort = ("date", True)
    column_filters = ["category"]
    page_size = 200


admin.add_view(AdminView(Log, db.session, category="Update"))
