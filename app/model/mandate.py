# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Mandate(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("mandates", lazy="dynamic")
    )
    constituency_id = db.Column(db.Integer, db.ForeignKey("constituency.id"))
    constituency = db.relationship(
        "Constituency", backref=db.backref("mandates", lazy="dynamic")
    )
    startdate = db.Column(db.DateTime)
    enddate = db.Column(db.DateTime)
    role = db.Column(db.String(2000))


class AdminView(View):
    column_default_sort = [("representative.name", False), ("constituency.name", False)]
    column_filters = ["representative.name", "constituency.name", "role"]


admin.add_view(AdminView(Mandate, db.session, category="CRUD"))
