# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Source(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("sources", lazy="dynamic")
    )
    name = db.Column(db.String(2000))
    url = db.Column(db.String(2000))


class AdminView(View):
    column_default_sort = "name"
    column_filters = ["representative.name", "name"]


admin.add_view(AdminView(Source, db.session, category="CRUD"))
