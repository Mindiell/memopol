# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Batch(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    name = db.Column(db.String(2000))
    status = db.Column(db.String(1))
    duration = db.Column(db.Integer)
    message = db.Column(db.Text)


class AdminView(View):
    column_default_sort = ("date", True)
    column_descriptions = {"duration": "Duration in milliseconds"}
    column_exclude_list = ["message"]
    column_filters = ["name", "status"]


admin.add_view(AdminView(Batch, db.session, category="Update"))
