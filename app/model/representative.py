# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Representative(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # General data
    name = db.Column(db.String(2000))
    picture = db.Column(db.String(2000))
    gender = db.Column(db.String(1))
    birthdate = db.Column(db.DateTime)
    citizenship_id = db.Column(db.Integer, db.ForeignKey("country.id"))
    citizenship = db.relationship(
        "Country", backref=db.backref("representatives", lazy="dynamic")
    )
    last_update = db.Column(db.DateTime)

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = "name"
    column_exclude_list = ["picture"]
    column_filters = ["name", "citizenship.name"]


admin.add_view(AdminView(Representative, db.session, category="CRUD"))
