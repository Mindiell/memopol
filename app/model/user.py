# encoding: utf-8

from hashlib import sha512

from app import admin, db
from app.model.model import Model, View


def get_user(user_id):
    return User.query.get(user_id)


class User(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(2000))
    password_hash = db.Column(db.String(128))
    email = db.Column(db.String(2000))
    active = db.Column(db.Boolean)
    admin = db.Column(db.Boolean)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = sha512(password.encode("utf-8")).hexdigest()

    def check_password(self, password):
        return self.password_hash == sha512(password.encode("utf-8")).hexdigest()

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    def get_id(self):
        return str(self.id)


class AdminView(View):
    column_default_sort = "login"
    column_exclude_list = ["password_hash"]

    def on_model_change(self, form, model, is_created):
        # if new password is set, hash it before saving it
        if len(form.password_hash.data) < 128:
            model.password_hash = sha512(
                form.password_hash.data.encode("utf-8")
            ).hexdigest()


# Model will be automatically managed through flask-admin module
admin.add_view(AdminView(User, db.session))
