# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Country(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(5))
    name = db.Column(db.String(2000))

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = "code"
    column_filters = ["name"]


admin.add_view(AdminView(Country, db.session, category="CRUD"))
