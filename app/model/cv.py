# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Cv(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("cvs", lazy="dynamic")
    )
    what = db.Column(db.String(2000))


class AdminView(View):
    column_default_sort = "representative.name"
    column_filters = ["representative.name"]


admin.add_view(AdminView(Cv, db.session, category="CRUD"))
