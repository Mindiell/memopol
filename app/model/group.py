# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Group(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000))
    type_id = db.Column(db.Integer, db.ForeignKey("type.id"))
    type = db.relationship("Type", backref=db.backref("groups", lazy="dynamic"))

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = "name"
    column_filters = ["type.name"]


admin.add_view(AdminView(Group, db.session, category="CRUD"))
