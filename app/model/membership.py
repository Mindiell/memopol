# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Membership(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("memberships", lazy="dynamic")
    )
    group_id = db.Column(db.Integer, db.ForeignKey("group.id"))
    group = db.relationship("Group", backref=db.backref("memberships", lazy="dynamic"))
    startdate = db.Column(db.DateTime)
    enddate = db.Column(db.DateTime)
    role = db.Column(db.String(2000))


class AdminView(View):
    column_default_sort = [("representative.name", False), ("group.name", False)]
    column_filters = ["representative.name", "group.name", "role"]


admin.add_view(AdminView(Membership, db.session, category="CRUD"))
