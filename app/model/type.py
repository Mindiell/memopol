# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Type(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000))

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = "name"
    column_filters = ["name"]


admin.add_view(AdminView(Type, db.session, category="CRUD"))
