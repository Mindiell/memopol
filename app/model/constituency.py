# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Constituency(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000))
    code = db.Column(db.String(200))
    type_id = db.Column(db.Integer, db.ForeignKey("type.id"))
    type = db.relationship("Type")
    country_id = db.Column(db.Integer, db.ForeignKey("country.id"))
    country = db.relationship("Country")

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = "name"
    column_filters = ["name", "type.name", "country.name"]


admin.add_view(AdminView(Constituency, db.session, category="CRUD"))
