# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Address(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("addresses", lazy="dynamic")
    )
    name = db.Column(db.String(2000))
    country_id = db.Column(db.Integer, db.ForeignKey("country.id"))
    country = db.relationship("Country")
    zipcode = db.Column(db.String(200))
    city = db.Column(db.String(2000))
    street = db.Column(db.String(2000))
    building = db.Column(db.String(200))
    floor = db.Column(db.String(200))
    office = db.Column(db.String(200))
    latitude = db.Column(db.String(200))
    longitude = db.Column(db.String(200))


class AdminView(View):
    column_default_sort = [("name", False), ("representative.name", False)]
    column_filters = ["name", "representative.name", "country.name"]


admin.add_view(AdminView(Address, db.session, category="CRUD"))
