# encoding: utf-8
"""
This module imports models to allow alembic and flask to find them.
"""

from app.model.batch import Batch
from app.model.country import Country
from app.model.representative import Representative
from app.model.type import Type
from app.model.address import Address
from app.model.constituency import Constituency
from app.model.cv import Cv
from app.model.group import Group
from app.model.link import Link
from app.model.log import Log
from app.model.mail import Mail
from app.model.mandate import Mandate
from app.model.membership import Membership
from app.model.phone import Phone
from app.model.position import Position
from app.model.source import Source
from app.model.user import User
