# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Mail(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("mails", lazy="dynamic")
    )
    name = db.Column(db.String(2000))
    mail = db.Column(db.String(2000))

    def __repr__(self):
        return self.name


class AdminView(View):
    column_default_sort = [("representative.name", False), ("name", False)]
    column_filters = ["name", "representative.name"]


admin.add_view(AdminView(Mail, db.session, category="CRUD"))
