# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Position(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship(
        "Representative", backref=db.backref("positions", lazy="dynamic")
    )
    extract = db.Column(db.Text)
    source_name = db.Column(db.String(2000))
    source = db.Column(db.String(2000))
    date = db.Column(db.DateTime)


class AdminView(View):
    column_default_sort = ("date", True)
    column_filters = ["representative.name", "source_name"]


admin.add_view(AdminView(Position, db.session, category="CRUD"))
