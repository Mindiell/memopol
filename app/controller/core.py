# encoding: utf-8

from datetime import datetime
import random

from flask import g, render_template
from sqlalchemy.sql.expression import func, desc

from app import db
from app.controller.controller import Controller
from app.model.representative import Representative as RepresentativeModel


class Core(Controller):
    def home(self):
        # Selecting "today's featured representative"
        random.seed(int(datetime.strftime(datetime.now(), "%Y%m%d")))
        try:
            nb = random.randint(
                1,
                db.session.query(func.count("*"))
                .select_from(RepresentativeModel)
                .scalar(),
            )
            g.representative = (
                RepresentativeModel.query.order_by("id")
                .limit(nb)
                .from_self()
                .order_by(RepresentativeModel.id.desc())
                .first()
            )
        except ValueError:
            # There is no representative...
            g.representative = None
        g.today = datetime.today()

        return render_template("core/home.html")

    def about(self):
        return render_template("core/about.html")
