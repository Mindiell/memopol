# encoding: utf-8

from datetime import datetime

from flask import g, render_template
from sqlalchemy.sql.expression import func, or_

from app.controller.controller import Controller
from app.form.representative import RepresentativeForm
from app.model.constituency import Constituency as ConstituencyModel
from app.model.group import Group as GroupModel
from app.model.mandate import Mandate as MandateModel
from app.model.membership import Membership as MembershipModel
from app.model.representative import Representative as RepresentativeModel


class Representative(Controller):
    def index(self):
        g.form = RepresentativeForm()
        # Select between each actual constituency
        g.form.constituency.choices = [(0, "All")] + [
            (constituency.id, "%s (%s)" % (constituency.code, constituency.name))
            for constituency in ConstituencyModel.query.join(MandateModel)
            .filter(MandateModel.enddate >= datetime.today())
            .filter(MandateModel.startdate <= datetime.today())
            .order_by("name")
            .all()
        ]
        # Select between each actual group
        g.form.group.choices = [(0, "All")] + [
            (group.id, group.name)
            for group in GroupModel.query.join(MembershipModel)
            .filter(MembershipModel.enddate >= datetime.today())
            .filter(MembershipModel.startdate <= datetime.today())
            .order_by("name")
            .all()
        ]
        representatives = RepresentativeModel.query.join(MandateModel).join(
            MembershipModel
        )
        if g.form.validate_on_submit():
            if g.form.name.data != "":
                representatives = representatives.filter(
                    RepresentativeModel.name.like("%%%s%%" % g.form.name.data)
                )
            if g.form.constituency.data != 0:
                representatives = (
                    representatives.filter(
                        MandateModel.constituency_id == g.form.constituency.data
                    )
                    .filter(MandateModel.enddate >= datetime.today())
                    .filter(MandateModel.startdate <= datetime.today())
                )
            if g.form.group.data != 0:
                representatives = (
                    representatives.filter(
                        MembershipModel.group_id == g.form.group.data
                    )
                    .filter(MembershipModel.enddate >= datetime.today())
                    .filter(MembershipModel.startdate <= datetime.today())
                )
        g.representatives = representatives.distinct().limit(10).all()
        g.today = datetime.today()

        return render_template("representative/index.html")
