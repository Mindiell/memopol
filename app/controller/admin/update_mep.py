# encoding: utf-8

from datetime import datetime
import json
import os
from subprocess import call

from flask import current_app, g, request, redirect, url_for
from flask_admin import BaseView, expose

from app.model.batch import Batch as BatchModel
from app.model.country import Country as CountryModel
from app.model.address import Address as AddressModel
from app.model.constituency import Constituency as ConstituencyModel
from app.model.cv import Cv as CvModel
from app.model.group import Group as GroupModel
from app.model.link import Link as LinkModel
from app.model.log import Log as LogModel
from app.model.mandate import Mandate as MandateModel
from app.model.mail import Mail as MailModel
from app.model.membership import Membership as MembershipModel
from app.model.phone import Phone as PhoneModel
from app.model.representative import Representative as RepresentativeModel
from app.model.type import Type as TypeModel
from app.controller.admin.batch import (
    add_log,
    end_batch,
    set_batch_message,
    start_batch,
)
from app.controller.admin.json_reader import json_reader


class UpdateMepView(BaseView):
    @expose("/", methods=["GET", "POST"])
    def index(self):
        name = "Update MEP"
        g.batch = BatchModel.query.filter_by(name=name).filter_by(status="R").first()
        if request.method == "POST":
            if g.batch is None:
                g.batch = start_batch(current_app, name, update_mep)
                return redirect(url_for("update_mep.index"))
        return self.render("admin/update_mep.html")


def update_mep(app, *args):
    name = args[0]
    with app:
        # Delete old files if necessary
        try:
            os.remove("/tmp/ep_meps.json.lz")
        except FileNotFoundError:
            pass
        try:
            os.remove("/tmp/ep_meps.json")
        except FileNotFoundError:
            pass

        # Download latest dump from parltrack
        set_batch_message(app, name, "Downloading dump")
        add_log(app, name, "Downloading dump")
        call(
            ["wget", "http://parltrack.org/dumps/ep_meps.json.lz", "-P", "/tmp/", "-q"]
        )

        # Extract datas from it
        set_batch_message(app, name, "Extracting datas")
        add_log(app, name, "Extracting datas")
        cwd = os.getcwd()
        os.chdir("/tmp")
        call(["lzip", "-d", "ep_meps.json.lz"])
        os.chdir(cwd)

        # For each mep from parltrack file
        total_mep = 0
        active_mep = 0
        errors = []
        for mep in json_reader("/tmp/ep_meps.json"):
            total_mep += 1
            if mep["active"]:
                # with open("%s.json" % mep["Name"]["full"], "w") as f:
                #    json.dump(mep, f, indent=4)
                try:
                    if mep.get("Name", {}).get("full", "") == "":
                        continue
                    active_mep += 1
                    message = "Processing %s<br />%d active(s) MEP on %d" % (
                        mep["Name"]["full"],
                        active_mep,
                        total_mep,
                    )
                    set_batch_message(app, name, message)
                    add_log(app, name, "Processing %s" % mep["Name"]["full"])
                    representative_format = convert_mep_to_representative(mep)
                    representative = RepresentativeModel(
                        name=representative_format["name"],
                        picture=representative_format["picture"],
                        gender=representative_format["gender"],
                        birthdate=representative_format["birthdate"],
                        last_update=datetime.now(),
                    )
                    representative.save()
                    for address in representative_format["addresses"]:
                        AddressModel(
                            representative_id=representative.id,
                            name=address["name"],
                            zipcode=address["zipcode"],
                            city=address["city"],
                            street=address["street"],
                            building=address["building"],
                            floor=address["floor"],
                            office=address["office"],
                            latitude=address["latitude"],
                            longitude=address["longitude"],
                            # TODO: country
                        ).save()
                    for cv in representative_format["cv"]:
                        CvModel(
                            representative_id=representative.id,
                            what=cv,
                        ).save()
                    for phone in representative_format["phones"]:
                        PhoneModel(
                            representative_id=representative.id,
                            name=phone["name"],
                            number=phone["number"],
                        ).save()
                    for mail in representative_format["mails"]:
                        MailModel(
                            representative_id=representative.id,
                            # TODO: Give a name to mails
                            # name = mail["name"],
                            mail=mail,
                        ).save()
                    for link in representative_format["links"]:
                        LinkModel(
                            representative_id=representative.id,
                            name=link["name"],
                            url=link["url"],
                        ).save()
                    for group in representative_format["groups"]:
                        group_type = TypeModel.query.filter_by(
                            name=group["type"]
                        ).first()
                        if group_type is None:
                            group_type = TypeModel(name=group["type"])
                            group_type.save()
                        group_group = (
                            GroupModel.query.filter_by(type_id=group_type.id)
                            .filter_by(name=group["name"])
                            .first()
                        )
                        if group_group is None:
                            group_group = GroupModel(
                                name=group["name"],
                                type_id=group_type.id,
                            )
                            group_group.save()
                        MembershipModel(
                            representative_id=representative.id,
                            group_id=group_group.id,
                            role=group["role"],
                            startdate=group["start"],
                            enddate=group["end"],
                        ).save()
                    for mandate in representative_format["mandates"]:
                        mandate_type = TypeModel.query.filter_by(
                            name=mandate["type"]
                        ).first()
                        if mandate_type is None:
                            mandate_type = TypeModel(name=mandate["type"])
                            mandate_type.save()
                        mandate_constituency = (
                            ConstituencyModel.query.filter_by(type_id=mandate_type.id)
                            .filter_by(name=mandate["name"])
                            .first()
                        )
                        if mandate_constituency is None:
                            mandate_constituency = ConstituencyModel(
                                name=mandate["name"],
                                type_id=mandate_type.id,
                                code=mandate["code"],
                            )
                            mandate_constituency.save()
                        MandateModel(
                            representative_id=representative.id,
                            constituency_id=mandate_constituency.id,
                            role=mandate["role"],
                            startdate=mandate["start"],
                            enddate=mandate["end"],
                        ).save()

                except KeyError as e:
                    errors.append("KeyError: %s" % e)
                    with open("/tmp/%s_%s.json" % (mep["Name"]["full"], e), "w") as f:
                        json.dump(mep, f, indent=4)
                except ValueError as e:
                    errors.append("ValueError: %s" % e)
                    with open("/tmp/%s_%s.json" % (mep["Name"]["full"], e), "w") as f:
                        json.dump(mep, f, indent=4)

        message = "Processed %d active(s) MEP on %d\n%s" % (
            active_mep,
            total_mep,
            "\n".join(errors),
        )
        set_batch_message(app, name, message)

        # Clean files
        try:
            os.remove("/tmp/ep_meps.json.lz")
        except FileNotFoundError:
            pass
        try:
            os.remove("/tmp/ep_meps.json")
        except FileNotFoundError:
            pass

        end_batch(app, "Update MEP")


def convert_mep_to_representative(mep):
    representative = {}
    # Id
    mep_id = mep.get("UserID", "")
    if mep_id == "":
        # This should be impossible
        print("Passing MEP without ID field !", err=True)
        return
    representative["mep_id"] = mep_id
    # Name
    name = mep.get("Name", {})
    if name == {}:
        # This should be impossible
        return
    full_name = name.get("full_name", "")
    if full_name == "":
        full_name = " ".join((name.get("family", ""), name.get("sur", ""))).strip()
    representative["name"] = full_name

    # Photo
    representative["picture"] = mep.get("Photo", "")

    # Gender
    representative["gender"] = mep.get("Gender", "U")

    # Birth
    representative["birthdate"] = mep.get("Birth", {}).get("date", None)
    if representative["birthdate"]:
        representative["birthdate"] = datetime.strptime(
            representative["birthdate"],
            "%Y-%m-%dT%H:%M:%S",
        )
    representative["birthplace"] = mep.get("Birth", {}).get("place", "")

    # TODO: Death
    # TODO: Assistants
    # TODO: Declarations of Participation
    # TODO: Financial Declarations
    # TODO: activities ???
    # TODO: Postal address ???

    # CV
    raw_cv = mep.get("CV", [])
    cv = []
    if type(raw_cv) is list:
        for line in raw_cv:
            for subline in line.split(". "):
                if subline != "":
                    cv.append(subline)
    else:
        for key in raw_cv:
            cv.append(key)
            if type(raw_cv[key]) is list:
                for subline in raw_cv[key]:
                    if subline != "":
                        cv.append(subline)
    # representative["cv"] = "\n".join(cv)
    representative["cv"] = cv

    # Origin id (Parliament ID)
    representative["origin_id"] = representative["mep_id"]

    # Origin URL (Parliament URL)
    meta = mep.get("meta", {})
    representative["origin_url"] = meta.get("url", "")

    # Last update
    last_update = meta.get("updated", "")
    if last_update == "":
        last_update = meta.get("created", "")
        if last_update == "":
            representative["last_update"] = datetime.strftime(
                datetime.now(), "%Y-%m-%dT%H:%M:%S"
            )
    if "last_update" not in representative:
        representative["last_update"] = last_update[:19]

    # Addresses and phones
    representative["addresses"] = []
    representative["phones"] = []
    for address_name in mep.get("Addresses", {}):
        parltrack_address = mep["Addresses"][address_name]
        if "Address" in parltrack_address:
            representative_address = {
                "name": "",
                "country": "",
                "zipcode": parltrack_address["Address"].get(
                    "Zip", parltrack_address["Address"].get("Zip1", "")
                ),
                "city": parltrack_address["Address"].get("City", ""),
                "street": parltrack_address["Address"].get("Street", ""),
                "building": parltrack_address["Address"].get("Building", ""),
                "floor": "",
                "office": parltrack_address["Address"].get("Office", ""),
                "latitude": "",
                "longitude": "",
            }
            if "Organization" in parltrack_address["Address"]:
                representative_address["name"] = "%s (%s)" % (
                    parltrack_address["Address"]["Organization"],
                    address_name,
                )
            else:
                representative_address["name"] = address_name
            if address_name == "Brussels":
                representative_address["country"] = "Belgium"
            elif address_name == "Strasbourg":
                representative_address["country"] = "France"
            representative["addresses"].append(representative_address)
        if "Phone" in parltrack_address:
            representative["phones"].append(
                {
                    "name": "%s phone" % address_name,
                    # TODO: Convert phone number
                    "number": parltrack_address["Phone"].replace(" ", ""),
                }
            )
        if "Fax" in parltrack_address:
            representative["phones"].append(
                {
                    "name": "%s fax" % address_name,
                    # TODO: Convert fax number
                    "number": parltrack_address["Fax"].replace(" ", ""),
                }
            )

    # Mails
    representative["mails"] = []
    for mail in mep.get("Mail", []):
        representative["mails"].append(mail)

    # Links
    representative["links"] = []
    for url in mep.get("Facebook", []):
        representative["links"].append(
            {
                "name": "Facebook",
                "url": url,
            }
        )
    for url in mep.get("Twitter", []):
        representative["links"].append(
            {
                "name": "Twitter",
                "url": url,
            }
        )
    for url in mep.get("Homepage", []):
        representative["links"].append(
            {
                "name": "Homepage",
                "url": url,
            }
        )

    # Mandates
    representative["groups"] = []
    # Parties
    for party in [z for z in mep.get("Constituencies", []) if z is not None]:
        representative["groups"].append(
            {
                "name": party.get("party", ""),
                # Maybe try to find out a system to create a code for parties ?
                "code": "",
                "type": "Party",
                "role": "Member",
                "country": party.get("country", ""),
                "start": datetime.strptime(party["start"], "%Y-%m-%dT%H:%M:%S"),
                "end": datetime.strptime(party["end"], "%Y-%m-%dT%H:%M:%S"),
            }
        )
    # Mandates
    representative["mandates"] = []
    # Committees
    for committee in [z for z in mep.get("Committees", []) if z is not None]:
        representative["mandates"].append(
            {
                "name": committee.get("Organization", ""),
                "code": committee.get("abbr", ""),
                "type": "European Parliament Committee",
                "role": committee.get("role", ""),
                "country": committee.get("country", ""),
                "start": datetime.strptime(committee["start"], "%Y-%m-%dT%H:%M:%S"),
                "end": datetime.strptime(committee["end"], "%Y-%m-%dT%H:%M:%S"),
            }
        )
    # Groups
    for group in [z for z in mep.get("Groups", []) if z is not None]:
        if type(group.get("groupid", "")) is list:
            representative["mandates"].append(
                {
                    "name": group.get("Organization", ""),
                    "code": group.get("groupid", [""])[0],
                    "type": "European Parliament Group",
                    "role": group.get("role", ""),
                    "country": group.get("country", ""),
                    "start": datetime.strptime(group["start"], "%Y-%m-%dT%H:%M:%S"),
                    "end": datetime.strptime(group["end"], "%Y-%m-%dT%H:%M:%S"),
                }
            )
        else:
            representative["mandates"].append(
                {
                    "name": group.get("Organization", ""),
                    "code": group.get("groupid", ""),
                    "type": "European Parliament Group",
                    "role": group.get("role", ""),
                    "country": group.get("country", ""),
                    "start": datetime.strptime(group["start"], "%Y-%m-%dT%H:%M:%S"),
                    "end": datetime.strptime(group["end"], "%Y-%m-%dT%H:%M:%S"),
                }
            )
    # Delegations
    for delegation in [z for z in mep.get("Delegations", []) if z is not None]:
        representative["mandates"].append(
            {
                "name": delegation.get("Organization", ""),
                "code": delegation.get("abbr", ""),
                "type": "European Parliament Delegation",
                "role": delegation.get("role", ""),
                "country": "",
                "start": datetime.strptime(delegation["start"], "%Y-%m-%dT%H:%M:%S"),
                "end": datetime.strptime(delegation["end"], "%Y-%m-%dT%H:%M:%S"),
            }
        )

    return representative
