# encoding: utf-8

from app.controller.admin.update_countries import ImportCountriesView
from app.controller.admin.update_mep import UpdateMepView

# By commenting a route, the associated update will become unavailable in admin pages
admin_routes = (
    (ImportCountriesView, "Import Countries", "import_countries", "Update"),
    (UpdateMepView, "Update MEP", "update_mep", "Update"),
)
