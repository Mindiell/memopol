# encoding: utf-8

import datetime
from threading import Thread

from app.model.batch import Batch as BatchModel
from app.model.log import Log as LogModel


def start_batch(app, name, function, *args):
    with app.app_context():
        batch = BatchModel(name=name)
        batch.status = "R"
        batch.date = datetime.datetime.now()
        batch.duration = 0
        batch.message = "Batch started"
        batch.save()
        thread = Thread(target=function, args=(app.app_context(), name, args))
        thread.daemon = True
        thread.start()
        add_log(app.app_context(), name, "Starting batch")
        return batch


def set_batch_message(app, name, message):
    with app:
        batch = BatchModel.query.filter_by(name=name).filter_by(status="R").first()
        batch.message = message
        batch.save()


def end_batch(app, name):
    with app:
        batch = BatchModel.query.filter_by(name=name).filter_by(status="R").first()
        batch.status = "F"
        duration = datetime.datetime.now() - batch.date
        batch.duration = duration.seconds * 1000 + int(duration.microseconds / 1000)
        batch.message += "\nBatch ended"
        batch.save()
        add_log(app, name, "Ending batch")


def add_log(app, category, message):
    with app:
        LogModel(
            date=datetime.datetime.now(), category=category, message=message
        ).save()
