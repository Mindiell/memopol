# encoding: utf-8

import csv
from io import StringIO

from flask import current_app, g, request, redirect, url_for
from flask_admin import BaseView, expose

from app.form.admin import ImportCountriesForm
from app.model.batch import Batch as BatchModel
from app.model.country import Country as CountryModel
from app.model.log import Log as LogModel
from app.controller.admin.batch import (
    add_log,
    end_batch,
    set_batch_message,
    start_batch,
)


class ImportCountriesView(BaseView):
    @expose("/", methods=["GET", "POST"])
    def index(self):
        name = "Import Countries"
        g.batch = BatchModel.query.filter_by(name=name).filter_by(status="R").first()
        g.form = ImportCountriesForm()
        if g.form.validate_on_submit():
            if g.batch is None:
                reader = csv.reader(
                    StringIO(request.files["filename"].read().decode("utf-8"))
                )
                g.batch = start_batch(current_app, name, import_countries, reader)
                return redirect(url_for("import_countries.index"))
        g.log = (
            LogModel.query.filter_by(category=name)
            .order_by(LogModel.date.desc())
            .first()
        )
        return self.render("admin/import_countries.html")


def import_countries(app, *args):
    name = args[0]
    with app:
        for row in args[1][0]:
            if row[0].lower() == "code":
                continue
            country = CountryModel.query.filter_by(code=row[0]).first()
            if country is not None:
                if country.name != row[1]:
                    set_batch_message(
                        app, name, "Updating country %s to %s" % (country.name, row[1])
                    )
                    add_log(
                        app, name, "Updating country %s to %s" % (country.name, row[1])
                    )
                    country.name = row[1]
                    country.save()
            else:
                set_batch_message(app, name, "Adding country %s" % row[1])
                add_log(app, name, "Adding country %s" % row[1])
                country = CountryModel(code=row[0], name=row[1])
                country.save()
        end_batch(app, name)
