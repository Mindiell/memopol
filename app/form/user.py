# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired


class UserLoginForm(FlaskForm):
    login = StringField(_("Login"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])


class UserRegisterForm(FlaskForm):
    login = StringField(_("Login"), validators=[DataRequired()])
    email = StringField(_("Email"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])
