# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField
from wtforms.validators import DataRequired


class RepresentativeForm(FlaskForm):
    name = StringField(_("Name"))
    constituency = SelectField(_("Constituency"), coerce=int)
    group = SelectField(_("Group"), coerce=int)
