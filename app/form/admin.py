# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import FileField
from wtforms.validators import DataRequired


class ImportCountriesForm(FlaskForm):
    filename = FileField(_("File to import"), validators=[DataRequired()])
