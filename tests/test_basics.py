# encoding: utf-8

import unittest

import server


class TestBasic(unittest.TestCase):
    def setUp(self):
        self.client = server.app.test_client()

    def test_loading_homepage(self):
        result = self.client.get("/")
        self.assertIn("Homepage", str(result.data))
