# encoding: utf-8
"""
Server module.

Initializes Flask application and extensions and runs it.
"""

import flask

from app import admin, api, babel, db, login_manager, migrate
from app.model.user import get_user
from app.routes import apis, routes

try:
    from command import commands
except ImportError:
    # no command
    commands = ()


app = flask.Flask(__name__, template_folder="app/view")
app.config.from_object("config")
try:
    app.config.from_envvar("APP_CONFIG")
except RuntimeError:
    # APP_CONFIG is not set
    pass
app.config.from_envvar("ENVIRONMENT_CONFIG", silent=True)
if "JINJA_ENV" in app.config:
    app.jinja_env.trim_blocks = app.config["JINJA_ENV"]["TRIM_BLOCKS"]
    app.jinja_env.lstrip_blocks = app.config["JINJA_ENV"]["LSTRIP_BLOCKS"]

# Loading routes
for route in routes:
    if len(route) < 3:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=route[2])
# Loading API routes
for route in apis:
    api.add_resource(route[1], route[0])

# Initialisation of extensions
admin.init_app(app)
api.init_app(app)
babel.init_app(app)
db.init_app(app)
login_manager.init_app(app)
migrate.init_app(app, db)

# Manage commands
for command in commands:
    app.cli.add_command(command)


# Manage locale
@babel.localeselector
def get_locale():
    """
    Return locale for flask-babel extension.
    """
    return flask.session.get("locale", flask.current_app.config["BABEL_DEFAULT_LOCALE"])


# Manage user
@login_manager.user_loader
def load_user(user_id):
    """
    Load user from its id.
    """
    return get_user(user_id)


if __name__ == "__main__":
    # Running application
    app.run(
        debug=app.config["DEBUG"],
        host=app.config["HOST"],
        port=app.config["PORT"],
    )
