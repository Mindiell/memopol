# encoding: utf-8
"""
Default configuration
"""

import os

DEBUG = False
HOST = "0.0.0.0"
PORT = 5000
SECRET_KEY = "Choose a secret key"

JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
# MySQL example
# SQLALCHEMY_DATABASE_URI = "mysql://username:password@server/db"
# SQLite example
# SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3")
SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3")
SQLALCHEMY_TRACK_MODIFICATIONS = False

# defining Babel settings
BABEL_DEFAULT_LOCALE = "en"

# Languages available
AVAILABLE_LANGUAGES = {
    "en": "English",
}
