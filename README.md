# Memopol

[![pipeline status](https://framagit.org/Mindiell/memopol/badges/develop/pipeline.svg)](https://framagit.org/Mindiell/memopol/-/commits/develop) [![coverage report](https://framagit.org/Mindiell/memopol/badges/develop/coverage.svg)](https://framagit.org/Mindiell/memopol/-/commits/develop)

Political Memory (Memopol) is a toolbox designed to help citizens follow the activity and voting records of their representatives in order to help them influence decision and policy-making.

This project is a rebuild from original [lqdn designed memopol project](https://git.laquadrature.net/memopol).



## Installation

Project is on dev stage, thus installation is certainly buggy and application is useless for now.

1. clone this repository
2. python3 -m venv .venv
3. source .venv/bin/activate
4. pip install -r requirements.txt
5. [optional] pip install -r requirements-dev.txt
6. export FLAKS_APP=server.py
7. [optional] export FLASK_ENV=development
8. flask db init
9. flask db upgrade
10. flask run

Once installed and running, you can simply go to http://localhost:5000/admin and start to add some representative by hand.



## Contributing

Project is on dev stage, so rules are still not totally fixed.

### Git workflow

Use [this tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) to understand our workflow

### Git commit message

As long as we can, we use those prefixes whenever we commit.

* doc: When the commit is about documentation
* feat: When the commit is about a feature
* fix: When the commit is about fixing an issue
* ref: When the commit is about refactoring the code
* test: When the commit is about testing

And try to use [these rules](https://nitayneeman.com/posts/understanding-semantic-commit-messages-using-git-and-angular/) too.

